(ns plf02.core)

(defn función-associative?-1
  [a]
  (associative? a))

(defn función-associative?-2
  [a]
  (associative? a))

(defn función-associative?-3
  [a]
  (associative? a))

(función-associative?-1 [\a \b \c])
(función-associative?-2  {:a 1 :b 2 :c 3})
(función-associative?-3 (vector 1 2 3))

(defn función-boolean?-1
  [a]
  (boolean? a)) 

(defn función-boolean?-2
  [a]
  (boolean? a))

(defn función-boolean?-3
  [a]
  (boolean? a))

(función-boolean?-1 true)
(función-boolean?-2 false)
(función-boolean?-2 nil)

(defn función-char?-1
  [a]
  (char? a))

(defn función-char?-2
  [a]
  (char? a))

(defn función-char?-3
  [a]
  (char? a))

(función-char?-1 \a)
(función-char?-2 [1 2])
(función-char?-2 "a")

(defn función-coll?-1
  [a]
  (coll? a))

(defn función-coll?-2
  [a]
  (coll? a))

(defn función-coll?-3
  [a]
  (coll? a))

(función-coll?-1 [1 2 3])
(función-coll?-2 {\a \b \c \d})
(función-coll?-3 #{:a \a :b \b})

(defn función-decimal?-1
  [a]
 (decimal? a))

(defn función-decimal?-2
  [a]
  (decimal? a))

(defn función-decimal?-3
  [a]
  (decimal? a))

(función-decimal?-1 1.2M)
(función-decimal?-2 \a)
(función-decimal?-2 1)

(defn función-double?-1
  [a]
  (double? a))

(defn función-double?-2
  [a]
  (double? a))

(defn función-double?-3
  [a]
  (double? a))

(función-double?-1 1.0)
(función-double?-2 1)
(función-double?-3 1.2M)

(defn función-float?-1
  [a]
  (float? a))

(defn función-float?-2
  [a]
  (float? a))

(defn función-float?-3
  [a]
  (float? a))

(función-float?-1 1.0)
(función-float?-2 1)
(función-float?-3 1.0M)

(defn función-ident?-1
  [a]
  (ident? a))

(defn función-ident?-2
  [a]
  (ident? a))

(defn función-ident?-3
  [a]
  (ident? a))

(función-ident?-1 :abcd)
(función-ident?-2 'abcd)
(función-ident?-3  \a)

(defn función-indexad?-1
  [a ]
  (indexed? a ))

(defn función-indexad?-2
  [a ]
  (indexed? a ))

(defn función-indexad?-3
  [a]
  (indexed? a))

(función-indexad?-1 [1 2 3 4 5])
(función-indexad?-2 [\a \b \c])
(función-indexad?-3 [])

(defn función-int?-1
  [a]
  (int? a))

(defn función-int?-2
  [a]
  (int? a))

(defn función-int?-3
  [a]
  (int? a))

(función-int?-1 42)
(función-int?-2 4.2)
(función-int?-3 \a)


(defn función-integer?-1
  [a]
  (integer? a))

(defn función-integer?-2
  [a]
  (integer? a))

(defn función-integer?-3
  [a]
  (integer? a))

(función-integer?-1 12)
(función-integer?-2 13N)
(función-integer?-3 13.0)

(defn función-keyword?-1
  [a]
  (keyword? a))

(defn función-keyword?-2
  [a]
  (keyword? a))

(defn función-keyword?-3
  [a]
  (keyword? a))

(función-keyword?-1 :abcd)
(función-keyword?-2 :a:b:c:d)
(función-keyword?-3 :74)

(defn función-list?-1
  [a]
(list? a))

(defn función-list?-2
  [a]
  (list? a))
  
(defn función-list?-3
  [a]
  (list? a))
  
(función-list?-1 '( \a \b \c))
(función-list?-2 (list \a \b \c \d))
(función-list?-3 [])

(defn función-map-entry?-1
    [a]
    (map-entry? a))

(defn función-map-entry?-2
  [a]
  (map-entry? a))

(defn función-map-entry?-3
  [a]
  (map-entry? a))

(función-map-entry?-1 (first {:a \a :b \b}))
(función-map-entry?-2 (first {:a 1}))
(función-map-entry?-3  {:a \a :b \b})

(defn función-map?-1
  [a]
  (map? a))

(defn función-map?-2
  [a]
  (map? a))

(defn función-map?-3
  [a]
  (map? a))

(función-map?-1 {:a \a :b \b :c \c})
(función-map?-2 (hash-map :a \a :b \b :c \c))
(función-map?-3 { })

(defn función-nat-int?-1
  [a]
  (nat-int? a))

(defn función-nat-int?-2
  [a]
  (nat-int? a))

(defn función-nat-int?-3
  [a]
  (nat-int? a))

(función-nat-int?-1 1)
(función-nat-int?-2 0)
(función-nat-int?-2 -1)

(defn función-number?-1
  [a]
  (number? a))

(defn función-number?-2
  [a]
  (number? a))

(defn función-number?-3
  [a]
  (number? a))

(función-number?-1 1)
(función-number?-2 -1)
(función-number?-1 \a)

(defn función-pos-int?-1
  [a]
  (pos-int? a))

(defn función-pos-int?-2
  [a]
  (pos-int? a))

(defn función-pos-int?-3
  [a]
  (pos-int? a))

(función-pos-int?-1 1)
(función-pos-int?-2 123462213776678)
(función-pos-int?-2 -1)

(defn función-ratio?-1
  [a]
  (ratio? a))

(defn función-ratio?-2
  [a]
  (ratio? a))

(defn función-ratio?-3
  [a]
  (ratio? a))

(función-ratio?-1 22/7)
(función-ratio?-2 2.2)
(función-ratio?-3 \a)

(defn función-rational?-1
  [a]
  (rational? a))

(defn función-rational?-2
  [a]
  (rational? a))

(defn función-rational?-3
  [a]
  (rational? a))

(función-rational?-1 22/7)
(función-rational?-2 1)
(función-rational?-2 1.0)

(defn función-seq?-1
  [a]
  (seq? a))

(defn función-seq?-2
  [a]
  (seq? a))

(defn función-seq?-3
  [a]
  (seq? a))

(función-seq?-1 '(1 2 3 4))
(función-seq?-2  1)
(función-seq?-2  (seq [1]))

(defn función-seqable?-1
[a]
(seqable? a))

(defn función-seqable?-2
  [a]
  (seqable? a))

(defn función-seqable?-3
  [a]
  (seqable? a))

(función-seqable?-1 [\a])
(función-seqable?-2 {\a \b \c \d})
(función-seqable?-3 '())

(defn función-sequencial?-1
  [a]
  (sequential? a))

(defn función-sequencial?-2
  [a]
  (sequential? a))

(defn función-sequencial?-3
  [a]
  (sequential? a))

(función-sequencial?-1 '(1 2 3 4))
(función-sequencial?-1 [1 2 3 4 5])
(función-sequencial?-1 '(\a \b \c \d))

(defn función-set?-1
  [a]
  (set? a))

(defn función-set?-2
  [a]
  (set? a))

(defn función-set?-3
  [a]
  (set? a))

(función-set?-1 #{1 2 3 4})
(función-set?-2 (hash-set 1 2 3 4))
(función-set?-3 [])

(defn función-some?-1
[a]
  (some? a))

(defn función-some?-2
  [a]
  (some? a))

(defn función-some?-3
  [a]
  (some? a))

(función-some?-1 [\a \b \c])
(función-some?-2 #{1 2 3 4})
(función-some?-3 {:a 2 :b 4})

(defn función-string?-1
  [a]
  (string? a))

(defn función-string?-2
  [a]
  (string? a))

(defn función-string?-3
  [a]
  (string? a))

(función-string?-1 "hola")
(función-string?-2 "")
(función-string?-3 \a)

(defn función-symbol?-1
  [a]
  (symbol? a))

(defn función-symbol?-2
  [a]
  (symbol? a))


(defn función-symbol?-3
  [a]
  (symbol? a))

(función-symbol?-1 'a)
(función-symbol?-2 1)
(función-symbol?-3 :a)

(defn función-vector?-1
  [a]
  (vector? a))

(defn función-vector?-2
  [a]
  (vector? a))

(defn función-vector?-3
  [a]
  (vector? a))

(función-vector?-1 [\a \b \c])
(función-vector?-2 '[])
(función-vector?-2 (vector 1 2 3))

(defn función-drop-1
  [a b]
  (drop a b))

(defn función-drop-2
  [a b]
  (drop a b))

(defn función-drop-3
  [a b]
  (drop a b))

(función-drop-1 1 [1 2 3 4])
(función-drop-2 2 [\a \b \c \d])
(función-drop-3 5 [1 2 3 4 5])

(defn función-drop-last-1
  [a]
(drop-last a))

(defn función-drop-last-2
  [a b]
(drop-last a b))

(defn función-drop-last-3
  [a b]
  (drop-last a b))

(función-drop-last-1 [1 2 3 4])
(función-drop-last-2 2 {:a \a :b \b :c \c :d \d})
(función-drop-last-2 3 [1 2 3 4])

(defn función-drop-while-1
  [a b]
  (drop-while a b))

(defn función-drop-while-2
  [a b]
  (drop-while a b))

(defn función-drop-while-3
  [a b]
  (drop-while a b))

(función-drop-while-1 neg? [1 2 3 4 -1])
(función-drop-while-1 pos? [1 2 3 4 -1])
(función-drop-while-1 pos-int? [1 2 3 4 -1.1 2.2])

(defn función-every?-1
  [a b]
(every? a b))

(defn función-every?-2
  [a b]
  (every? a b))

(defn función-every?-3
  [a b]
  (every? a b))

(función-every?-1 #{1  2} [1 2])
(función-every?-1 #{1 "uno" 2 "dos"} [1 2])
(función-every?-1 #{1 2} #{1})

(defn función-filterv-1
  [a b]
  (filterv a b))

(defn función-filterv-2
  [a b]
  (filterv a b))

(defn función-filterv-3
  [a b]
  (filterv a b))

(función-filterv-1 even?(range 10))
(función-filterv-2 even? (range -10 50))
(función-filterv-3 even? (range -10))

(defn función-group-by-1
  [a b]
  (group-by a b))

(defn función-group-by-2
  [a b]
  (group-by a b))

(defn función-group-by-3
  [a b]
  (group-by a b))

(función-group-by-1 count ["a" "as" "asd" "aa" "asdf" "qwer"])
(función-group-by-2 odd? (range 10))
(función-group-by-3 :category [{:category "a" :id 1}
                               {:category "a" :id 2}
                               {:category "b" :id 3}])

(defn función-iterate-1
  [a b]
  (iterate a b))

(defn función-iterate-2
  [a b]
  (iterate a b))


(defn función-iterate-3
  [a b ]
  (iterate a b ))

(función-iterate-1 inc 5)
(función-iterate-2 dec 12)
(take 5 (función-iterate-3 inc 5))

(defn función-keep-1
  [a b]
  (keep a b))

(defn función-keep-2
  [a b]
  (keep a b))

(defn función-keep-3
  [a b]
  (keep a b))

(función-keep-1 even?(range 4 10))
(función-keep-2 seq [() [] '(1 2 3) [:a :b] nil])
(función-keep-3 {:a 1, :b 2, :c 3} [:a :b :d])

(defn función-keep-indexed-1
  [a b ]
  (keep-indexed a b))

(defn función-keep-indexed-2
  [a b]
  (keep-indexed a b))

(defn función-keep-indexed-3
  [a b]
(keep-indexed a b))

(función-keep-indexed-1 #(if (odd? %1) %2) [:a :b :c :d :e])
(función-keep-indexed-2 #(if (pos? %2) %1) [-9 0 29 -7 45 3 -8])
(función-keep-indexed-2 #(when (< % 2) (str % %2)) [:a :b :c])

(defn función-map-indexed-1
  [a b]
  (map-indexed a b))

(defn función-map-indexed-2
  [a b]
  (map-indexed a b))

(defn función-map-indexed-3
  [a b]
  (map-indexed a b))

(función-map-indexed-1 vector "HOLA")
(función-map-indexed-2 hash-map "HOLA")
(función-map-indexed-2 list [:a :b :c])

(defn función-mapcat-1
  [a b]
  (mapcat a b))

(defn función-mapcat-2
  [a b]
(mapcat a b))

(defn función-mapcat-3
  [a b c]
  (mapcat a b c))

(función-mapcat-1 reverse [[3 2 1 0] [6 5 4] [9 8 7]])
(función-mapcat-2 #(repeat 2 %) [1 2])
(función-mapcat-3 list [:a :b :c] [1 2 3])

(defn función-mapv-1
  [a b]
  (mapv a b))

(defn función-mapv-2
  [a b c]
  (mapv a b c))

(defn función-mapv-3
  [a b ]
  (mapv a b ))

(función-mapv-1 inc [1 2 3 4 5] )
(función-mapv-2 + [1 2 3] [4 5 6])
(función-mapv-3 dec [1 2 3] )

(defn función-merge-with-1
[a b c]
  (merge-with a b c))

(defn función-merge-with-2
[a b c]
(merge-with a b c))

(defn función-merge-with-3
  [a b c]
  (merge-with a b c))

(función-merge-with-1 + {:a 1} {:a 2})
(función-merge-with-2 merge {:a {:b 1}} {:c {:d 2}})
(función-merge-with-2 - {:a 6} {:a 3})

(defn función-not-any?-1
  [a b]
  (not-any? a b))

  (defn función-not-any?-2
  [a b]
  (not-any? a b))

(defn función-not-any?-3
  [a b]
  (not-any? a b))

(función-not-any?-1 odd? '(2 4 6))
(función-not-any?-2 nil? [true false false])
(función-not-any?-3 odd? '(1 2 3))

(defn función-not-every?-1
[a b]
 (not-every? a b))

(defn función-not-every?-2
  [a b]
  (not-every? a b))
  
(defn función-not-every?-3
    [a b]
    (not-every? a b))


(función-not-every?-1 odd? '(2 4 6))
(función-not-every?-2 nil? [true false false])
(función-not-every?-3 odd? '(1 2 3))

(defn función-partition-by-1
[a b]
  (partition-by a b))

(defn función-partition-by-2
[a b]
(partition-by a b))

(defn función-partition-by-3
  [a b]
  (partition-by a b))

(función-partition-by-1  #(= 3 %) [1 2 3 4 5])
(función-partition-by-2 odd?[1 1 1 2 2 3 3])
(función-partition-by-2 even? [1 1 1 2 2 3 3])

(defn función-reduce-kv-1
  [a b c]
  (reduce-kv a b c))

(defn función-reduce-kv-2
  [a b c]
  (reduce-kv a b c))

(defn función-reduce-kv-3
  [a b c]
  (reduce-kv a b c))

(función-reduce-kv-1 #(assoc %1 %3 %2) {} {:a 1 :b 2 :c 3})
(función-reduce-kv-2 #(assoc %1 %3 %2) {} {:a \a :b \b :c \c})
(función-reduce-kv-2 #(assoc %1 %3 %2) {} {})

(defn función-remove-1
  [a b]
  (remove a b))

(defn función-remove-2
  [a b]
  (remove a b))

(defn función-remove-3
  [a b]
  (remove a b))

(función-remove-1 pos? [1 -2 2 -1 3 7 0])
(función-remove-2  nil? [1 nil 2 nil 3 nil])
(función-remove-3  neg? [1 -2 2 -1 3 7 0])

(defn función-reverse-1
 [a]
  (reverse a))

(defn función-reverse-2
  [a]
  (reverse a))

(defn función-reverse-3
  [a]
  (reverse a))

(función-reverse-1 '(1 2 3 4))
(función-reverse-2 "HOLA")
(función-reverse-3 (vector \a \b \c))

(defn función-some-1
  [a b]
  (some a b))

(defn función-some-2
  [a b]
(some a b))

(defn función-some-3
  [a b]
  (some a b))

(función-some-1 true? [false false false])
(función-some-2 #(= 5 %) [1 2 3 4 5])
(función-some-3 {2 "two" 3 "three"} [nil 3 2])

(defn función-sort-by-1
  [a b]
  (sort-by a b))

(defn función-sort-by-2
  [a b]
  (sort-by a b))

(defn función-sort-by-3
  [a b c]
  (sort-by a b c))

(función-sort-by-1 count ["aaa" "bb" "c"])
(función-sort-by-2 first [[1 2] [2 2] [2 3]])
(función-sort-by-3 first > [[1 2] [2 2] [2 3]])

(defn función-split-with-1
  [a b]
  (split-with a b))

(defn función-split-with-2
  [a b]
  (split-with a b))

(defn función-split-with-3
  [a b]
  (split-with a b))

(función-split-with-1 (partial >= 3) [1 2 3 4 5])
(función-split-with-2 (partial > 10) [1 2 3 2 1])
(función-split-with-3 #{:c} [:a :b :c :d])

(defn función-take-1
  [a b]
  (take a b))

(defn función-take-2
  [a b]
  (take a b))

(defn función-take-3
  [a b]
  (take a b))

(función-take-1 3 '(1 2 3 4 5 6))
(función-take-2 3 [1 2 3 4 5 6])
(función-take-3 1 nil)

(defn funcion-take-last-1
  [a b]
  (take-last a b))

(defn funcion-take-last-2
  [a b]
(take-last a b))

(defn funcion-take-last-3
  [a b]
  (take-last a b))

(funcion-take-last-1 2 [1 2 3 4])
(funcion-take-last-2 2 nil)
(funcion-take-last-3 -1 [1])

(defn función-take-while-1
   [a b]
  (take-while a b))

(defn función-take-while-2
   [a b]
  (take-while a b))

(defn función-take-while-3
   [a b]
  (take-while a b))

(función-take-while-1 neg? [-2 -1 0 1 2 3])
(función-take-while-2 (partial > 5.5) [6 9.5 7 9.3 4 3.1 0 4])
(función-take-while-3 int? [5 6.5 6 true false])

(defn función-update-1
  [p c a]
  (update p c a))

(defn función-update-2
  [p c a d e]
  (update p c a d e))

(defn función-update-3
  [p c d]
  (update p c d))

(función-update-1 {:name "James" :age 26} :age inc)
(función-update-2 [1 2 3 4 5 6 7] 2 + 10 5)
(función-update-3  {:name "James" :age 26} :age dec)

(defn función-update-in-1
  [p a c]
  (update-in p a c))

(defn función-update-in-2
  [p a b c]
  (update-in p a b c))

(defn función-update-in-3
  [p a c d b]
  (update-in p a c d b))
(función-update-in-1 {:name "James" :age 26} [:age] inc)
(función-update-in-2 {:name "Alejandra" :age 27 :altura 142 :peso 50}
                     [:altura] + 20)
(función-update-in-3 {:a 3} [:a] / 3 20)
